Basic Server configs for all UGCS servers

Instructions are provided in the instructions file, which should should mirror the instructions on the wiki

The actual config files are in the config folder.  Config files that vary between servers will be suffixed with a dash.  The contents of these files must be manually added to the relevant files on the server
