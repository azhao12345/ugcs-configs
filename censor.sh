
git filter-branch --force --index-filter 'git rm --cached --ignore-unmatch config/etc/sssd/sssd.conf' --prune-empty --tag-name-filter cat -- --all
git filter-branch --force --index-filter 'git rm --cached --ignore-unmatch config/etc/ssh/sshd_config' --prune-empty --tag-name-filter cat -- --all
git filter-branch --force --index-filter 'git rm --cached --ignore-unmatch zinc-config/etc/ssh/sshd_config' --prune-empty --tag-name-filter cat -- --all

git filter-branch --force --index-filter 'git rm --cached --ignore-unmatch gold-config/etc/mediawiki/LocalSettings.php' --prune-empty --tag-name-filter cat -- --all
git filter-branch --force --index-filter 'git rm --cached --ignore-unmatch gold-config/var/www/mediawiki/LocalSettings.php' --prune-empty --tag-name-filter cat -- --all
git filter-branch --force --index-filter 'git rm --cached --ignore-unmatch gold-config/var/www/selfservicepassword/conf/config.inc.php' --prune-empty --tag-name-filter cat -- --all

git filter-branch --force --index-filter 'git rm --cached --ignore-unmatch cadmium-config/etc/dovecot/dovecot-ldap.conf.ext' --prune-empty --tag-name-filter cat -- --all
git filter-branch --force --index-filter 'git rm --cached --ignore-unmatch cadmium-config/etc/postfix/ldap-aliases.cf' --prune-empty --tag-name-filter cat -- --all
git filter-branch --force --index-filter 'git rm --cached --ignore-unmatch cadmium-config/etc/postfix/ldap-recipients.cf' --prune-empty --tag-name-filter cat -- --all

git filter-branch --force --index-filter 'git rm --cached --ignore-unmatch zinc-config/etc/apache2/sites-enabled/000-default.conf' --prune-empty --tag-name-filter cat -- --all
